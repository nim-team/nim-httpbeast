# Example file for upstream/metadata.
# See https://wiki.debian.org/UpstreamMetadata for more info/fields.
# Below an example based on a github project.

# Bug-Database: https://github.com/<user>/nim-httpbeast/issues
# Bug-Submit: https://github.com/<user>/nim-httpbeast/issues/new
# Changelog: https://github.com/<user>/nim-httpbeast/blob/master/CHANGES
# Documentation: https://github.com/<user>/nim-httpbeast/wiki
# Repository-Browse: https://github.com/<user>/nim-httpbeast
# Repository: https://github.com/<user>/nim-httpbeast.git
